import UIKit

enum AppConstant {

}

extension NSNotification {

  /// Interesting events within the app life cycle.
  enum Firework {
    static let SignedIn = "onSignInCompleted"
  }

}

extension UIColor {

  enum Firework {
    static let primary = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
    static let secondary = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
  }

}

extension UserDefaults {

  enum Firework {
    static let isTutorialShown = "isTutorialShown"
    static let firstLaunch = "firstLaunch"
  }

}
