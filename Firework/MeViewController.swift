import UIKit

class MeViewController: UIViewController {

  @IBOutlet weak var image: UIImageView!

  override func viewDidLoad() {
    super.viewDidLoad()
    image.layer.magnificationFilter = kCAFilterNearest
  }
}
