//
//  ErrorHandler.swift
//  Firework
//
//  Created by Kent Liau on 12/27/16.
//  Copyright © 2016 Kent Liau. All rights reserved.
//

import UIKit

protocol ErrorHandler {
  func handle(error: Error)
}

extension ErrorHandler {
  func handle(error: Error) {
    print(error.localizedDescription)
  }
}

extension ErrorHandler where Self: UIViewController {
  func handle(error: Error) {
    let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
    let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
    alert.addAction(action)
    present(alert, animated: true, completion: nil)
  }
}
