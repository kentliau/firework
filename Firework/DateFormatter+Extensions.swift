//
//  DateFormatter+Extensions.swift
//  Firework
//
//  Created by Kent Liau on 12/26/16.
//  Copyright © 2016 Kent Liau. All rights reserved.
//

import Foundation

// Because DateFormatter is expensive, so having a/some shared through out the application
// can save some CPU resource
extension DateFormatter {

  static let shortDateAndTime: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .short
    formatter.timeStyle = .short
    return formatter
  }()

  static let dayMonthAndYear: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "MM/dd/yyyy"
    return formatter
  }()

  static let monthAndYear: DateFormatter = {
    let formatter = DateFormatter()
    formatter.setLocalizedDateFormatFromTemplate("MMMyyyy")
    return formatter
  }()

}
