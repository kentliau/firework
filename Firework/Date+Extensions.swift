//
//  Date+Extensions.swift
//  Firework
//
//  Created by Kent Liau on 12/26/16.
//  Copyright © 2016 Kent Liau. All rights reserved.
//

import Foundation

extension Date {

  /// Prints a string representation for the date with the given formatter
  func string(with format: DateFormatter) -> String {
    return format.string(from: self)
  }

  /// Creates an `NSDate` from the given string and formatter. Nil if the string couldn't be parsed
  init?(string: String, formatter: DateFormatter) {
    guard let date = formatter.date(from: string) else { return nil }
    self.init(timeIntervalSince1970: date.timeIntervalSince1970)
  }
  
}
