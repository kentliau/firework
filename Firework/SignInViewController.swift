// WARN: This class know too much about firebase, authentication, and other state
/*
import UIKit
import Firebase // This thing in here, it must be joking with me

class SignInViewController: UIViewController {

    // TODO: Connect me please.
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!


    override func viewDidLoad() {
        super.viewDidLoad()

        // If user already signed in to Firebase.
        if let user = FIRAuth.auth()?.currentUser {
            self.signedIn(user)
        }
    }

    // TODO: Connect me please.
    @IBAction func didTapSignIn(_ sender: AnyObject) {
        // Sign in with credentials.
        guard let email = emailField.text, let password = passwordField.text else { return }

        // TODO: Facebook login it as well.
        FIRAuth.auth()?.signIn(withEmail: email, password: password) {
            (user: FIRUser?, error: Error?) in

            if let error = error {
                // TODO: Sign in error handling
                // Like wrong password combination.
                print(error.localizedDescription)
                return
            }
            // TODO: Are you sure you want to force unwrap?
            self.signedIn(user)
        }

    }

    @IBAction func didTapSignUp(_ sender: AnyObject) {
        guard let email = emailField.text, let password = passwordField.text else { return }

        FIRAuth.auth()?.createUser(withEmail: email, password: password) {
            (user: FIRUser?, error: Error?) in

            if let error = error {
                // TODO: Sign up error handling
                print(error.localizedDescription)
                return
            }

            self.setDisplayName(user)
        }


    }

    // TODO: Password reset
    @IBAction func didRequestPasswordReset(_ sender: AnyObject) {
        let prompt = UIAlertController(title: nil, message: "Email:", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) {
            (action) in
            // WARN: Give a reason of using !
            let userInput = prompt.textFields![0].text
            // WARN: Another !
            if userInput!.isEmpty {
                return
            }
            FIRAuth.auth()?.sendPasswordReset(withEmail: userInput!) {
                (error) in
                if let error = error {
                    // TODO: Come on, you can handle better than this
                    print(error.localizedDescription)
                    return
                }
            }
        }

        prompt.addTextField(configurationHandler: nil)
        prompt.addAction(okAction)
        present(prompt, animated: true, completion: nil)

    }

    // TODO: The Post-SignedUp routines
    //
    func setDisplayName(_ user: FIRUser?) {
        let changeRequest = user?.profileChangeRequest()
        changeRequest?.displayName = user?.email?.components(separatedBy: "@")[0]
        changeRequest?.commitChanges() {
            (error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            self.signedIn(FIRAuth.auth()?.currentUser)
        }
    }

    // Post-SignedIn routines
    func signedIn(_ user: FIRUser?) {
        // TODO: Any analytic need to do?
        //MeasurementHelper.sendLoginEvent()

        AppState.sharedInstance.displayName = user?.displayName ?? user?.email
        AppState.sharedInstance.photoURL = user?.photoURL
        AppState.sharedInstance.signedIn = true

        let notificationName = Notification.Name(rawValue: Constants.NotificationKeys.SignedIn)
        NotificationCenter.default.post(name: notificationName, object: nil, userInfo: nil)

        // TODO: Bring user to the appropriate scene
        performSegue(withIdentifier: "SomeSegueYouWantToPerform,PutInConstants", sender: nil)

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
*/
