import Foundation


protocol WebService {

    var endpoint: String { get }

}

/// Our own web API services
struct FireworkWebService {



}

struct WebServiceError: Error {
}

// Location Based Notification Web Service
struct PostNotificationWebService {

    // Expose the available parameters as properties
    let radius: Double
    let lat: Double
    let long: Double

    struct Response {
        // like what kind of response is expected from this service
        // so we can take benefits of autocomplete
    }

    func start(finished: (Response?, WebServiceError?) -> Void) {

        // Asynchronously, in background

            // Either Dispatch yourself or use one of the Alamofire's asynchronous method
            assert(false, "Oh Oh")

            let response: Response  = Response()
            let error: WebServiceError? = nil

            finished(response, error)

        // Asynchronously
    }
}

// Device to Device Notification Web Service
struct PostNotificationToDeviceWebService {

    let receiver: String

    struct Response {

    }

    func start(finished: (Response?, WebServiceError?) -> Void) {

        // Async

            //callback
        // Async

    }
    
    
}

// TODO: Firebase directly in View Controller
// What if you decide to change from Firebase to another services?
/*
    Have all Firebase service map to our own service API call, so we abstract the complexity,
    and also break the tight dependency on Firework, also it is unit testable.
 
    You need Firebase through out your application, put it in the top level
*/

struct LoginWebService {}
struct RegisterWebService {}
struct FirejobWebService {}
struct UserWebService {}

