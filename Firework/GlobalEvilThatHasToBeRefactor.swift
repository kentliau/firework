import Foundation

enum Result<T> {
  case success(value: T)
  case failure(error: Error)
}

struct Token {
  let string: String
}

struct Credential {
  let email: String
  let password: String
}


func login(credential: Credential, completion: (Result<Token>) -> Void) {

  // send the credential to the server
  // ...
  // ...
  let statusCode = HTTPStatusCode.unauthorized
  let responseBody = "THE_TOKEN_CODE_FROM_SERVER"

  switch statusCode {
  case .accepted:
    let token = Token(string: responseBody)
    let result = Result.success(value: token)
    completion(result)
  case .unauthorized:
    let error = NSError(domain: "com.appserver.http",
                          code: statusCode.rawValue,
                      userInfo: [NSLocalizedDescriptionKey: responseBody])
    let result = Result<Token>.failure(error: error)
    completion(result)
  default:
    print("i don't handle this response code")
  }





}


let credential = Credential(email: "john@example.com",
                         password: "secret")

func someoneWhoWillCallExecuteOrCallTheseMethod() {

  login(credential: credential) { result in
    switch result {
    case .success(let token):
      print(token.string)
    case .failure(let error):
      print(error.localizedDescription)
    }
  }

}
