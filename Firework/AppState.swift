import Foundation

// The Automata State Machine
// This is application level state
// We use the state to decide which some high level stuff?

class AppState {

    static let sharedInstance = AppState()

    var signedIn = false
    var displayName: String?
    var photoURL: URL?


    // TODO: Setup a application observer for online state.
    // Firebase is able to handle itself for offline state.
    // But our push notification service (device to device and location based)
    // should handle ourself. Make sure our OneSignal return 200:OK, or we will
    // queue the push notification until we successfully send
    var isOnline = false

}
