import Foundation

struct Job: JobProtocol {
    let id: String
    
}

protocol JobProtocol {

  func fetch(id: String)
  func fetch()

  func save()
}

extension JobProtocol {
  func fetch(id: String) {}
  func fetch() {}
  func save() {}
}

struct JobViewModel {

  init(job: Job) {
    // injected job, prepare the job so it is presentable at view
  }
}


