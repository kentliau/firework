/*******************************************************************************
What should this class do

- [✅] It is the presenter/container for two child VCs
- [✅] It owns the two VCs
- [✅] It shows one VC at one time
- [✅] It handle switching of the two VC with segmented control
- [✅] It update the UI in tab bar when switching VC with segmented control
- [❓] It handle left nav bar item as data filtering
- [❓] It handle right nav bar item as data adding
- [❓] Memory management
- [✅] It expose some API
  - [✅] learn about current showing VC
  - [✅] switch the current VC, type safe way
  - [✅] retrieve the child VC
  - [✅] retrieve the current showing child VC

- [❌] Unit testable? Simple enough?
 
*******************************************************************************/

import UIKit

class HomeViewController: UIPageViewController {

  enum Page: Int {
    case Jobs = 0
    case Talents
  }

  var currentPage: Page {
    get {
      guard let page = Page(rawValue: jobsTalentsSegmentedControl.selectedSegmentIndex) else {
        fatalError("Cannot initialize the HomeViewController.Page for the provided raw value.")
      }
      return page
    }

    set(page) {
      setViewController(page: page)
    }
  }

  var currentViewController: UIViewController {
    get {
      switch currentPage {
      case .Jobs:
        return jobsViewController
      case .Talents:
        return talentsViewController
      }
    }
  }

  @IBOutlet weak var filterBarButtonItem: UIBarButtonItem!
  @IBOutlet weak var jobsTalentsSegmentedControl: UISegmentedControl!
  @IBOutlet weak var addBarButtonItem: UIBarButtonItem!
  @IBOutlet weak var jobsTalentsTabBarItem: MultiImagesTabBarItem!
  
  lazy var jobsViewController: JobsViewController = {
    // R.swift guaranteed its existence in compile time, no worry.
    R.storyboard.jobs.jobsViewController()!
  }()

  lazy var talentsViewController: TalentsViewController = {
    R.storyboard.talents.talentsViewController()!
  }()


}


// MARK: - Lifecycle
extension HomeViewController {

  override func viewDidLoad() {
    super.viewDidLoad()

    initializeResources()
    currentPage = .Jobs
  }

  override func didReceiveMemoryWarning() {
    // TODO: nil the other not currently showing VC
    // Log to server to analyze memory usage on different user
    switch currentPage {
    case .Jobs:
      Cheat.alert("nil out Talents VC")
    case .Talents:
      Cheat.alert("nil out Jobs VC")
    }
  }

}


// MARK: - IBActions
extension HomeViewController {

  @IBAction func filterBarButtonItemDidPressed(_ sender: UIBarButtonItem) {
    switch currentPage {
    case .Jobs:
      Cheat.actionSheet("Filter button pressed on Jobs, who should show the UI, who should own the UI, think hierarchically.")
    case .Talents:
      Cheat.actionSheet("Filter button pressed on Talents, who should show the UI, who should own the UI, think hierarchically.")
    }
  }

  @IBAction func jobsTalentsSegmentControlDidChanged(_ sender: UISegmentedControl) {
    guard let page = Page(rawValue: sender.selectedSegmentIndex) else {
      fatalError("Cannot initialize the HomeViewController.Page for the provided raw value.")
    }
    
    currentPage = page
  }

  @IBAction func addBarButtonItemDidPress(_ sender: UIBarButtonItem) {
    switch currentPage {
    case .Jobs:
      Cheat.alert("Add button pressed on Jobs, who should show the UI, who should own the UI, think hierarchically.")
    case .Talents:
      Cheat.alert("Add button pressed on Talents, who should show the UI, who should own the UI, think hierarchically.")

    }
  }


}


// MARK: - Private Helper
extension HomeViewController {

  fileprivate func setViewController(page: Page) {
    let viewController = [page.rawValue == 0 ? jobsViewController : talentsViewController]
    setViewControllers(viewController,
                       direction: .forward,
                       animated: false,
                       completion: nil)

    // Update segment control & tab bar to reflect the page
    jobsTalentsSegmentedControl.selectedSegmentIndex = page.rawValue

    jobsTalentsTabBarItem.useImages(ofIndex: page.rawValue)
    jobsTalentsTabBarItem.title = jobsTalentsSegmentedControl.titleForSegment(at: page.rawValue)
  }

  fileprivate func initializeResources() {
    jobsTalentsTabBarItem.addImages(normal: R.image.talent_tabBar_normal()!,
                                    selected: R.image.talent_tabBar_selected()!)
  }

}


// MARK: - UIPageViewController Data Source
extension HomeViewController: UIPageViewControllerDataSource {

  func pageViewController(_ pageViewController: UIPageViewController,
           viewControllerBefore viewController: UIViewController)
  -> UIViewController?
  {
    if viewController == jobsViewController {
      return talentsViewController
    }

    return nil
  }

  func pageViewController(_ pageViewController: UIPageViewController,
            viewControllerAfter viewController: UIViewController)
  -> UIViewController?
  {
    if viewController == talentsViewController {
      return jobsViewController
    }

    return nil
  }


}

