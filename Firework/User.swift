import Foundation

// DO REMEMBER, THESE MODEL STORE THE DATA STRUCTURE IN A VERY RAW WAY
// NO UIImage or anything complex beside of NSDate is still acceptable
// It closely resemble how data stored in database in a more type safe and
// structured way

public protocol UserProtocol {
  var uid: String { get }
  var email: String { get }

  var displayName: String { get set }
}

struct User: UserProtocol {

  let uid: String
  var email: String

  var displayName: String


}

struct UserViewModel {
  private var user: UserProtocol
}
