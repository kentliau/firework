import UIKit

class Cheat {
  class func alert(_ message: String) {
    let alertController = UIAlertController(title: nil,
                                          message: message,
                                   preferredStyle: .alert)

    alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))

    UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
  }

  class func actionSheet(_ message: String) {
    let alertController = UIAlertController(title: nil,
                                            message: message,
                                            preferredStyle: .actionSheet)

    alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))

    UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
  }
  
}
