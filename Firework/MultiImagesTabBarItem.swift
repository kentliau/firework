import UIKit

class MultiImagesTabBarItem: UITabBarItem {

  lazy var normalTabBarIcons: [UIImage] = {
    return [self.image!]
  }()

  lazy var selectedTabBarIcons: [UIImage] = {
    return [self.selectedImage!]
  }()

  func addImages(normal: UIImage, selected: UIImage) {
    self.normalTabBarIcons += [normal]
    self.selectedTabBarIcons += [selected]
  }

  func useImages(ofIndex index: Int) {
    self.image = self.normalTabBarIcons[index]
    self.selectedImage = self.selectedTabBarIcons[index]
  }

}
